#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <iostream>



class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:
	void initQueueMessage(SOCKET clientSocket);
	void removeUser(std::string nameToremove);
	void initAllUsers();
	void accept();
	void clientHandler(SOCKET clientSocket);

	SOCKET _serverSocket;
};


﻿#include "Server.h"
#include "Helper.h"
#include <exception>
#include <algorithm>
#include <thread>
#include <queue> 
#include <mutex> 
#include <fstream>
#include <vector>

using namespace std;

#define LOGIN_CODE "200"
#define SERVER_CODE "101"
#define ClientUpdate_CODE "204"

mutex mtx;
mutex mtx2;
string all_users;
queue <string> messageQueue;
vector <string> users;


Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}


Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}


void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::initQueueMessage(SOCKET clientSocket)
{
	string s_clientMessage;
	char clientMessage[MAX_SIZE];
	Helper::cleanBuffer(clientMessage);

	recv(clientSocket, clientMessage, MAX_SIZE, 0);
	s_clientMessage = clientMessage;

	mtx.lock();
	messageQueue.push(s_clientMessage);
	mtx.unlock();
}


void Server::removeUser(string nameToremove)
{
	for (int i = 0; i < users.size(); i++)
	{
		if (users[i] == nameToremove)
		{
			users.erase(users.begin() + i);
		}
	}
	initAllUsers();
}


void Server::initAllUsers()
{
	all_users.clear();
	for (int i = 0; i < users.size(); i++)
	{
		all_users += users[i] + "&";
	}
	cout<<"result:"<<all_users<<endl;
	mtx2.lock();
	if (all_users.size() != 0)
	{
		all_users.erase(all_users.size() - 1);
	}
	mtx2.unlock();
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	thread tC(&Server::clientHandler, this, client_socket);
	tC.detach();

}


void Server::clientHandler(SOCKET clientSocket)
{
	string NameFile;
	string s_clientMessage;
	string userName;
	string chat;
	try
	{
		while (true)
		{
			string chat;
			initQueueMessage(clientSocket);

			s_clientMessage = messageQueue.front();
			messageQueue.pop();

			if (Helper::getMessageCode(s_clientMessage) == LOGIN_CODE)//Login Message
			{
				userName = Helper::getNameUser(s_clientMessage);
				users.push_back(userName);
				initAllUsers();
				cout << all_users << endl;
				Helper::send_update_message_to_client(clientSocket, string(), string(), all_users);
			}
			else if (Helper::getMessageCode(s_clientMessage) == ClientUpdate_CODE)
			{
				if (Helper::getLenSecondUserName(s_clientMessage) != 0)
				{
					if (userName.compare(Helper::getSecondUserName(s_clientMessage)) < 0)
					{
						NameFile = userName + '&' + Helper::getSecondUserName(s_clientMessage) + ".txt";
					}
					else
					{
						NameFile = Helper::getSecondUserName(s_clientMessage) + '&' + userName + ".txt";
					}

					if (Helper::getMsg(s_clientMessage).size() != 0)
					{
						fstream file(NameFile, std::ios::out | std::ios::app);
						file << "&MAGSH_MESSAGE&&Author&" + userName + "&DATA&" + Helper::getMsg(s_clientMessage);
						file.close();
					}
					ifstream fileWrite(NameFile);
					getline(fileWrite, chat);
					fileWrite.close();

					Helper::send_update_message_to_client(clientSocket, chat, Helper::getSecondUserName(s_clientMessage), all_users);
				}
				else
				{
					Helper::send_update_message_to_client(clientSocket, string(), Helper::getSecondUserName(s_clientMessage), all_users);
				}
			}
		}
		closesocket(clientSocket);
	}
	catch (const std::exception & e)
	{
		removeUser(userName);
		cout << e.what() << endl;
		closesocket(clientSocket);
	}
}
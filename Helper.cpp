#include "Helper.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>

using std::string;


std::string Helper::getMessageCode(string s)
{
	return s.erase(LEN_CODE_MSG);
}


std::string Helper::getNameUser(std::string s)
{
	s.erase(0,5);
	return s;
}


int Helper::getLenSecondUserName(std::string s)
{

	s.erase(0, 3);
	s.erase(2);
	return stoi(s) ;
}


std::string Helper::getSecondUserName(std::string s)
{
	int lenSecUser = getLenSecondUserName(s);
	s.erase(0,5);
	s.erase(lenSecUser);
	return s;
}


std::string Helper::getMsg(std::string s)
{
	int lenName = getLenSecondUserName(s);
	s.erase(0, 5 + lenName + 5);
	return s;
}


void Helper::cleanBuffer(char *msgClient)
{
	int i = 0;
	for (i = 0; i < MAX_SIZE; i++)
	{
		msgClient[i] = '\0';
	}
}


void Helper::send_update_message_to_client(SOCKET sc, const string& file_content, const string& second_username, const string &all_users)
{
	//TRACE("all users: %s\n", all_users.c_str())
	const string code = std::to_string(MT_SERVER_UPDATE);
	const string current_file_size = getPaddedNumber(file_content.size(), 5);
	const string username_size = getPaddedNumber(second_username.size(), 2);
	const string all_users_size = getPaddedNumber(all_users.size(), 5);
	const string res = code + current_file_size + file_content + username_size + second_username + all_users_size + all_users;
	//TRACE("message: %s\n", res.c_str());
	sendData(sc, res);
}


// return string after padding zeros if necessary
string Helper::getPaddedNumber(int num, int digits)
{
	std::ostringstream ostr;
	ostr << std::setw(digits) << std::setfill('0') << num;
	return ostr.str();

}


// send data to socket
// this is private function
void Helper::sendData(SOCKET sc, std::string message)
{
	const char* data = message.c_str();

	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}